/**
 * 
 */

/**
 * LS05.2: Raumschiffe - Einführung in OOP in Java
 * Arbeitsblatt OOP Java
 * Aufgabe 1 - Kiste
 * 
 * Kistenklasse
 * 
 * @author Simon Asmus
 * @version 1.1 2022
 *
 */
public class Kiste {
    
    private double hoehe;
    private double tiefe;
    private double breite;
    private String farbe;
    
    public Kiste() {
	this.hoehe = 1.0;
	this.tiefe = 1.0;
	this.breite = 1.0;
	this.farbe = "Kistenfarben";
    }
    
    public Kiste (double h, double t, double b, String f) {
	this.hoehe = h;
	this.tiefe = t;
	this.breite = b;
	this.farbe = f;
    }
    
    public double getVolumen() {
	return this.hoehe * this.tiefe * this.breite;
    }
    
    public String getFarbe() {
	return this.farbe;
    }
    
}
