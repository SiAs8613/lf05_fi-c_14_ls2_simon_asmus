import java.util.ArrayList;
/**
 * LS05.2: Raumschiffe
 * 
 * A4.1: Implementierung der Klasse Raumschiff und Ladung
 * 
 * Raumschiffklasse
 * 
 * @author Simon Asmus
 * @version 1.0 2022
 *
 */
public class Raumschiff {

    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private String schiffsname;
    private int androidenAnzahl;
    private ArrayList<Ladung> Ladungsverzeichnis;
    
    public Raumschiff() {
	this.photonentorpedoAnzahl = 0;
	this.energieversorgungInProzent = 0;
	this.schildeInProzent = 0;
	this.huelleInProzent = 0;
	this.lebenserhaltungssystemeInProzent = 0;
	this.schiffsname = "-----";
	this.androidenAnzahl = 0;
	this.Ladungsverzeichnis = new ArrayList<Ladung>();
    }
    
    public Raumschiff(int ptA, int evIP, int sIP, int hIP, int lesIP, String sn, int aA) {
	this.photonentorpedoAnzahl = ptA;
	this.energieversorgungInProzent = evIP;
	this.schildeInProzent = sIP;
	this.huelleInProzent = hIP;
	this.lebenserhaltungssystemeInProzent = lesIP;
	this.schiffsname = sn;
	this.androidenAnzahl = aA;
	this.Ladungsverzeichnis = new ArrayList<Ladung>();
    }
    
    public int getPhotonentorpedoAnzahl() {
	return this.photonentorpedoAnzahl;
    }
    
    public void setPhotonentorpedoAnzahl(int ptA) {
	this.photonentorpedoAnzahl = ptA;
    }
    
    public int getenergieversorgungInProzent() {
	return this.energieversorgungInProzent;
    }
    
    public void setenergieversorgungInProzent(int evIP) {
	this.energieversorgungInProzent = evIP;
    }
    
    public int getschildeInProzent() {
	return this.schildeInProzent;
    }
    
    public void setschildeInProzent(int sIP) {
	this.schildeInProzent = sIP;
    }
    
    public int gethuelleInProzent() {
	return this.huelleInProzent;
    }
    
    public void sethuelleInProzent(int hIP) {
	this.huelleInProzent = hIP;
    }
    
    public int getlebenserhaltungssystemeInProzent() {
	return this.lebenserhaltungssystemeInProzent;
    }
    
    public void setlebenserhaltungssystemeInProzent(int lesIP) {
	this.lebenserhaltungssystemeInProzent = lesIP;
    }
    
    public String getschiffsname() {
	return this.schiffsname;
    }
    
    public void setschiffsname(String sn) {
	this.schiffsname = sn;
    }
    
    public int getandroidenAnzahl() {
	return this.androidenAnzahl;
    }
    
    public void setandroidenAnzahl(int aA) {
	this.androidenAnzahl = aA;
    }
    
    public void addLadung(Ladung neueLadung) {
	this.Ladungsverzeichnis.add(neueLadung);
    }
   
    
}
