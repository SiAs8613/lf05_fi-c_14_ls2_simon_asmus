/**
 * LS05.2: Raumschiffe - Einführung in OOP in Java
 * Arbeitsblatt OOP Java
 * Aufgabe 2 - Sparbuch
 * 
 * Testklasse für Sparbuchklasse
 * 
 * @author Simon Asmus
 * @version 1.1 2022
 *
 */
public class Test {
    public static void main(String[] args) {

	Sparbuch sb = new Sparbuch(1104711, 1000, 3);
	System.out.println("Kapital: " + sb.getKapital());
	sb.zahleEin(60000);

	System.out.println("Kapital: " + sb.getKapital());
	System.out.println("Ertrag nach 6 Jahr: " + sb.getErtrag(6));

	System.out.println("Kapital: " + sb.getKapital());
	sb.verzinse();
	System.out.println("Kapital: " + sb.getKapital());
	}
	
}
