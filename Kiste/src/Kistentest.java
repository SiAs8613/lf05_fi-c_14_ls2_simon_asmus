/**
 * 
 */

/**
 * LS05.2: Raumschiffe - Einführung in OOP in Java
 * Arbeitsblatt OOP Java
 * Aufgabe 1 - Kiste
 * 
 * Testklasse für Kistenklasse
 * 
 * @author Simon Asmus
 * @version 1.3 2022
 *
 */
public class Kistentest {

    public static void main(String[] args) {
	
	Kiste kiste1 = new Kiste(3.0, 3.0, 3.0, "rot");
	Kiste kiste2 = new Kiste(2.0, 2.0, 2.0, "blau");
	Kiste kiste3 = new Kiste(4.0, 4.0, 4.0, "gelb");
	
	System.out.println(" Volumen Kiste 1: " + kiste1.getVolumen());
	System.out.println(" Volumen Kiste 2: " + kiste2.getVolumen());
	System.out.println(" Volumen Kiste 3: " + kiste3.getVolumen());
	
	System.out.println(" Farbe Kiste 1: " + kiste1.getFarbe());
	System.out.println(" Farbe Kiste 2: " + kiste2.getFarbe());
	System.out.println(" Farbe Kiste 3: " + kiste3.getFarbe());

    }

}
