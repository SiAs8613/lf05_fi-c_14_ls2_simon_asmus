/**
 * LS05.2: Raumschiffe
 * 
 * A4.1: Implementierung der Klasse Raumschiff und Ladung
 * 
 * Ladungsklasse
 * 
 * @author Simon Asmus
 * @version 1.0 2022
 *
 */
public class Ladung {
    
    private String bezeichnung;
    private int menge;
    
    public Ladung() {
	this.bezeichnung = "-----";
	this.menge = 0;
    }
    
    public Ladung(String b, int m) {
	this.bezeichnung = b;
	this.menge = m;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public int getMenge() {
        return menge;
    }

    public void setMenge(int menge) {
        this.menge = menge;
    }

}
