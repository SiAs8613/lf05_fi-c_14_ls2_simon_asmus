/**
 * LS05.2: Raumschiffe - Einführung in OOP in Java
 * Arbeitsblatt OOP Java
 * Aufgabe 2 - Sparbuch
 * 
 * Sparbuchklasse
 * 
 * @author Simon Asmus
 * @version 1.1 2022
 *
 */
public class Sparbuch {

    private int kontonr;
    private double kapital;
    private double zinssatz;
    
    public Sparbuch() {
	this.kontonr = 0000000;
	this.kapital = 0.0;
	this.zinssatz = 0.0;	
    }
    
    public Sparbuch(int ktnr, double kptl, double znstz) {
	this.kontonr = ktnr;
	this.kapital = kptl;
	this.zinssatz = znstz;
    }
    
    public void zahleEin(double einzahlung) {
	this.kapital = this.kapital + einzahlung;
    }
    
    public void hebeAb(double auszahlung) {
	this.kapital = this.kapital - auszahlung;
    }
    
    public double getErtrag(int laufzeit) {
	double ertrag = 0.0;
	for (int i = 0; i <= laufzeit; i++) {
	    ertrag = this.kapital + this.kapital * this.zinssatz;
	}
	return ertrag;
    }
    
    public void verzinse() {
	double verzinsung = 0.0;
	for (int i = 0; i <= 12; i++) {
	    verzinsung = this.kapital + this.kapital * this.zinssatz;
	}
	this.kapital = verzinsung;
    }
    
    public int getKontonummer() {
	return this.kontonr;
    }
    
    public double getKapital() {
	return this.kapital;
    }
    
    public double getZinssatz() {
	return this.zinssatz;
    }
    
}
