/**
 * LS05.2: Raumschiffe
 * 
 * A4.1: Implementierung der Klasse Raumschiff und Ladung
 * 
 * Testklasse
 * 
 * @author Simon Asmus
 * @version 1.2 2022
 *
 */
public class TestRauLad {

    public static void main(String[] args) {
	
	Raumschiff Klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh'ta", 2);
	Raumschiff Romulaner = new Raumschiff(2, 100, 100, 100, 100, "IWS Khazara", 2);
	Raumschiff Vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni'Var", 5);
	Ladung Ladung1 = new Ladung("Ferengi Schneckensaft", 200);
	Ladung Ladung2 = new Ladung("Borg-Schrott", 5);
	Ladung Ladung3 = new Ladung("Rote Materie", 2);
	Ladung Ladung4 = new Ladung("Forschungssonde", 35);
	Ladung Ladung5 = new Ladung("Bat'leth Klingonen Schwert", 200);
	Ladung Ladung6 = new Ladung("Plasma-Waffe", 50);
	Ladung Ladung7 = new Ladung("Photonentorpedo", 3);
	
	Klingonen.addLadung(Ladung1);
	Klingonen.addLadung(Ladung5);
	Romulaner.addLadung(Ladung2);
	Romulaner.addLadung(Ladung3);
	Romulaner.addLadung(Ladung6);
	Vulkanier.addLadung(Ladung4);
	Vulkanier.addLadung(Ladung7);
	
    }
}
